﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;


/// <summary>
/// All objects that have this component can be destroyed by a bomb blast
/// </summary>
public class Destructable : MonoBehaviour {

    /// <summary>
    /// If set to false a bomb blast will be stopped after hitting this object
    /// </summary>
    public bool Penetratable = true;

    /// <summary>
    /// Set if the object should be destoryed after Destruct is called
    /// </summary>
    public bool DestroyGameObject = true;

    /// <summary>
    /// Set a function which should be called if the object gets destructed
    /// </summary>
    public UnityEvent OnDestruction = null;


    /// <summary>
    /// Make sure we destruct the object only once
    /// </summary>
    private bool _destructed = false;
    /// <summary>
    /// Can be called to destruct the object and calling the OnDestruction method
    /// </summary>
    public void Destruct()
    {
        if (_destructed) return;
        if (OnDestruction != null) OnDestruction.Invoke();


        Map map = GameObject.FindGameObjectWithTag("Map").GetComponent<Map>();
        map.Remove(Mathf.RoundToInt(transform.position.x), Mathf.RoundToInt(transform.position.z), gameObject);
        
        // TODO recycle gameobject
        if (DestroyGameObject) Destroy(gameObject);
        _destructed = true;
    }
}