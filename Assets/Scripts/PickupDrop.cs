﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// An object which has this component attached can drop one of the specified Drops by calling the Drop() function
/// </summary>
public class PickupDrop : MonoBehaviour {

    /// <summary>
    /// Probability with which the object drops anything (between 0 and 100)
    /// </summary>
    [Tooltip("Range 0 to 100")]
    public int DropRate = 30;

    [System.Serializable]
	public struct DropProbability
    {
        public GameObject Prefab;
        [Tooltip("Range 0 to 100")]
        public int Probability;
    }
    /// <summary>
    /// Possible Drops given by prefab and probablity
    /// Each probability scales with the sum over all given probabilities
    /// </summary>
    public DropProbability[] Drops;


    /// <summary>
    /// Drop an item by chance
    /// </summary>
    public void Drop()
    {
        if (Random.Range(0, 100) > DropRate) return;

        // compute sum of all probabilities (scale if they do not sum up to 100)
        float sum = 0;
        for (int i = 0; i < Drops.Length; i++) sum += Drops[i].Probability;

        // randomize between 0 and sum and find the according drop
        float rand = Random.Range(0, sum);
        sum = 0;
        for (int i = 0; i < Drops.Length; i++)
        {
            if (rand < Drops[i].Probability + sum)
            {
                // Drop the Item
                int x = Mathf.RoundToInt(transform.position.x);
                int y = Mathf.RoundToInt(transform.position.z);
                GameObject.FindGameObjectWithTag("Map").GetComponent<Map>().CreatePrefab(x, y, Drops[i].Prefab);
                break;
            }
            sum += Drops[i].Probability;
        }
    }
}
