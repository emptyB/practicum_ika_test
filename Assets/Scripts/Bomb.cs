﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// This behaviour contains the functionality of a bomb
/// </summary>
public class Bomb : MonoBehaviour {

    /// <summary>
    /// Time until the bomb explodes in sec
    /// </summary>
    public float ExplosionTime = 3;

    /// <summary>
    /// Bomb color should fade to the given color while the bomb timer runs out
    /// </summary>
    public Color FadeColor;

    /// <summary>
    /// Color of the bomb if it a remote bomb
    /// </summary>
    public Color RemoteBombColor;

    /// <summary>
    /// Fire that visualizes the explosion radius
    /// </summary>
    public GameObject FirePrefab;

    /// <summary>
    /// Explosion range - set by placing player
    /// </summary>
    private int _explosionRange;
    public int GetExplosionRange { get { return _explosionRange; } }

    /// <summary>
    /// Owner of the Bomb
    /// </summary>
    private Player _owner;

    /// <summary>
    /// Avoid stack overflow caused by chain explosions
    /// </summary>
    private bool _exploded = false;

    /// <summary>
    /// Save if the bomb is a remote bomb
    /// </summary>
    private bool _isRemote = false;
    public bool IsRemoteBomb { get { return _isRemote; } }

    /// <summary>
    /// Place the bomb
    /// </summary>
    /// <param name="owner">Owner of the bomb</param>
    public void PlaceBomb(Player owner, bool remote=false)
    {
        _owner = owner;
        _isRemote = remote;
        // save/set current bomb radius
        _explosionRange = _owner.BombRadius;

        // either start bomb timer or set the remote bomb for the player
        if (_isRemote) _owner.CurrentRemoteBomb = this;
        else StartCoroutine(bombTimer());
    }


    /// <summary>
    /// Coroutine that starts the bomb timer and fades its color
    /// </summary>
    /// <returns></returns>
    private IEnumerator bombTimer()
    {
        // decrease bomb counter
        _owner.BombCount--;
        // wait for given seconds until bomb explodes
        float time = ExplosionTime;
        Renderer rend = GetComponentInChildren<Renderer>();
        Color startCol = rend.material.color;
        while (ExplosionTime > 0)
        {
            yield return new WaitForEndOfFrame();
            ExplosionTime -= Time.deltaTime;
            // fade color
            rend.material.color = Color.Lerp(FadeColor, startCol, ExplosionTime / time);
        }
        Explode();
    }
    


    /// <summary>
    /// Let the bomb explode and cause destruction in the given range
    /// </summary>
    /// <returns>true if bomb could be detonated</returns>
    public void Explode()
    {
        if (_exploded) return;
        _exploded = true;

        Map map = GameObject.FindGameObjectWithTag("Map").GetComponent<Map>();
        int x = Mathf.RoundToInt(transform.position.x);
        int y = Mathf.RoundToInt(transform.position.z);

        // iterate through cells in all directions until Exposion range reached or bomb blast should be stopped
        destructCell(x, y, map);
        // save max values for visuals
        explodeInDir(x, y, 1, 0, map);
        explodeInDir(x, y, -1, 0, map);
        explodeInDir(x, y, 0, 1, map);
        explodeInDir(x, y, 0, -1, map);

        // remove from map and increase the bomb count (or remove remote bomb) of the owner
        if (_isRemote) _owner.CurrentRemoteBomb = null;
        else _owner.BombCount++;
        map.Remove(x, y, gameObject);

        // destroy with short delay to show fire/blast radius
        GetComponentInChildren<Renderer>().material.color = FadeColor;
        DestroyObject(gameObject, 0.2f);
    }
    


    /// <summary>
    /// Helper function: blast in a given direction (xAdd, yAdd)
    /// </summary>
    private void explodeInDir(int x, int y, int xAdd, int yAdd, Map map)
    {
        int range = 1;
        for (; range < _explosionRange + 1; range++)
        {
            // check if explosion should stop
            if (!destructCell(x + xAdd * range, y + yAdd * range, map))
            {
                range--;
                break;
            }
        }
        // add fire
        Instantiate(FirePrefab, transform).GetComponent<LineRenderer>().SetPosition(1, new Vector3(xAdd * (range+0f), 0, yAdd * (range+0f)));
    }


    /// <summary>
    /// Helper function to destruct all objects in a cell
    /// </summary>
    /// <param name="x">x pos of cell</param>
    /// <param name="y">y pos of cell</param>
    /// <returns>true if bomb blast should be stopped</returns>
    private bool destructCell(int x, int y, Map map)
    {
        HashSet<GameObject> cellEntries = map.GetCell(x, y);
        GameObject[] entries = new GameObject[cellEntries.Count]; cellEntries.CopyTo(entries);
        for(int i=0; i< entries.Length;i++)
        {
            if (entries[i] == gameObject) continue;
            Destructable destr = entries[i].GetComponent<Destructable>();
            // If the object has not a destructable component it is indestructable -> stop bomb blast
            if (destr == null) return false;
            destr.Destruct();
            // if the object is not penetratable stop bomb blast
            if (!destr.Penetratable) return false;
        }
        return true;
    }

}
