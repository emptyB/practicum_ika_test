﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// This Player behaviour is shared by human and AI players
/// It contains the main functionality of a player
/// </summary>
public class Player : MonoBehaviour {

    /// <summary>
    /// Prefab for a bomb
    /// </summary>
    public GameObject BombPrefab;
    
    /// <summary>
    /// Name of the player
    /// </summary>
    [HideInInspector]
    public string Name;

    /// <summary>
    /// Player settings
    /// </summary>
    public int BombCount = 1;
    public int BombRadius = 1;
    public float Speed = 1;
    /// <summary>
    /// Time remaining in which the player can set remote bombs
    /// </summary>
    public float RemoteBombTime = 0;

    /// <summary>
    /// Returns if the player is currently moving
    /// </summary>
    public bool IsMoving { get { return _moving; } }

    /// <summary>
    /// The current remote bomb or null if none exists
    /// </summary>
    [HideInInspector]
    public Bomb CurrentRemoteBomb;

    private Map _map;

    /// <summary>
    /// Movement variables start, target and current status
    /// </summary>
    private Vector3 _targetLocation;
    private Vector3 _startLocation;
    private bool _moving = false;

    // Use this for initialization
    void Start ()
    {
        _startLocation = _targetLocation = transform.position;
        _map = GameObject.FindGameObjectWithTag("Map").GetComponent<Map>();
	}
	
	// Update is called once per frame
	void Update () {

        // update remote bomb timer
        RemoteBombTime = Mathf.Max(RemoteBombTime - Time.deltaTime, 0);


        float dist = Vector3.Distance(_startLocation, _targetLocation);
        if (dist < float.Epsilon) return;


        // update movement
        Vector3 newPos = transform.position + (_targetLocation - _startLocation).normalized * Speed * Time.deltaTime;

        // pickup if the player moved half the distance
        if (Vector3.Distance(newPos, _targetLocation) < 0.5 &&
            Vector3.Distance(transform.position, _targetLocation) > 0.5)
        {
            foreach (GameObject obj in _map.GetCell(Mathf.RoundToInt(_targetLocation.x), Mathf.RoundToInt(_targetLocation.z)))
            {
                Pickup pickup = obj.GetComponent<Pickup>();
                if (pickup != null)
                {
                    pickup.PickUp(this);
                    obj.GetComponent<Destructable>().Destruct();
                    break;
                }
            }
        }
        
        
        if (Vector3.Distance(newPos, transform.position) > Vector3.Distance(_targetLocation, transform.position))
        {
            // overshoot -> new pos = target
            _map.Remove(Mathf.RoundToInt(_startLocation.x), Mathf.RoundToInt(_startLocation.z), gameObject, true);
            _startLocation = transform.position = _targetLocation;
            _moving = false;
        }
        else
        {
            transform.position = newPos;
        }
	}



    /// <summary>
    /// Move the player in a given direction
    /// </summary>
    /// <param name="dir">Movement direction</param>
    public void Move(Vector3 dir)
    {
        if (_moving) return;
        // set new target - make sure dir is normalized
        Vector3 target = _startLocation + dir.normalized;
        // check if movement is valid
        foreach(GameObject obj in _map.GetCell(Mathf.RoundToInt(target.x), Mathf.RoundToInt(target.z)))
        {
            if (obj.tag == "CollisionObject") return;
        }
        _targetLocation = target;
        _map.Add(Mathf.RoundToInt(_targetLocation.x), Mathf.RoundToInt(_targetLocation.z), gameObject);
        _moving = true;
    }


    /// <summary>
    /// Place a bomb at the current location
    /// </summary>
    public void PlaceBomb()
    {
        // Place bomb if count is larger than 0 and the no remote bomb is alive
        if (BombCount == 0 || CurrentRemoteBomb != null) return;
        Vector3 location = _targetLocation;
        // place bomb depending on current location either at start or target pos
        if (Vector3.Distance(_targetLocation, transform.position) > 0.5f) location = _startLocation;
        GameObject bomb = _map.CreatePrefab(Mathf.RoundToInt(location.x), Mathf.RoundToInt(location.z), BombPrefab);
        bomb.GetComponent<Bomb>().PlaceBomb(this, RemoteBombTime > 0);
    }


    /// <summary>
    /// Detonate the remote bomb if one has been placed / is alive
    /// </summary>
    public void DetonateRemoteBomb()
    {
        if (CurrentRemoteBomb == null) return;
        CurrentRemoteBomb.Explode();
    }

}
