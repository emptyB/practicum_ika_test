﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// The Map stores all object in the grid world and creates a new map based on a given string
/// </summary>
public class Map : MonoBehaviour {

    /// <summary>
    /// This string represents a quater of the default map (symmetric maps)
    /// This allows and simlifies later randomization processes
    /// </summary>
    public static string DEFAULT_MAPSTRING = 
      "x.-----\n"
     +".+-+-+-\n"
     +"-------\n"
     +"-+-+-+-\n"
     +"-------\n"
     +"-+-+-+-";

    /// <summary>
    /// Container for all static objects
    /// </summary>
    public Transform StaticContainer;
    /// <summary>
    /// Conatiner for all dynamic objects
    /// </summary>
    public Transform DynamicContainer;


    /// <summary>
    /// Wall prefabs
    /// </summary>
    public GameObject WallPrefab;
    public GameObject DestructableWallPrefab;

    /// <summary>
    /// Get the dimensions of the current map
    /// </summary>
    public Vector2 MapSize { get { return new Vector2(_currentMap.GetLength(0), _currentMap.GetLength(1)); } }

    /// <summary>
    /// Set of players currently alive
    /// </summary>
    private HashSet<GameObject> _currentPlayers;

    /// <summary>
    /// Current map where each entry is a list of objects added to the specific cell
    /// Some cells can conatin multiple objects e.g. player and bomb if placed
    /// </summary>
    private HashSet<GameObject>[,] _currentMap;

    /// <summary>
    /// Add given object to the map
    /// </summary>
    /// <param name="x">x pos of added object</param>
    /// <param name="y">y pos of added object</param>
    /// <param name="instance">object that should be added</param>
    public void Add(int x, int y, GameObject instance)
    {
        _currentMap[x, y] = _currentMap[x,y] ?? new HashSet<GameObject>();
        _currentMap[x, y].Add(instance);
        if (instance.GetComponent<Player>() != null)
        {
            _currentPlayers.Add(instance);
        }
    }

    /// <summary>
    /// Remove given object from the map
    /// </summary>
    /// <param name="x">x pos of the object</param>
    /// <param name="y">y pos of the object</param>
    /// <param name="instance">object that should be removed</param>
    /// <param name="playerStillAlive">if the player only updates its position it should not be removed</param>
    public void Remove(int x, int y, GameObject instance, bool playerStillAlive=false)
    {
        _currentMap[x, y].Remove(instance);
        if (instance.GetComponent<Player>() != null && !playerStillAlive)
        {
            _currentPlayers.Remove(instance);
        }
    }

    /// <summary>
    /// Returns all objects added to the given cell index
    /// </summary>
    /// <param name="x">x pos of the cell</param>
    /// <param name="y">y pos of the cell</param>
    /// <returns>set of objects added to cell x,y</returns>
    public HashSet<GameObject> GetCell(int x, int y)
    {
        return _currentMap[x,y] ?? new HashSet<GameObject>();
    }




    /// <summary>
    /// Creates a new map based on a given string 
    /// The mapString contains symbols ("x"=start, "+"=wall, "-"=destuctable wall, "."=space, "\n"=new line)
    /// The surringing walls are not contained in the given string
    /// </summary>
    /// <param name="mapString">a string that presenst a quater of the map (complete map is symmetric in x and y directions)</param>
    /// <returns>list of possible player spawns</returns>
    public List<Vector3> CreateMap(string mapString)
    {
        destroyMap();

        _currentPlayers = new HashSet<GameObject>();
        List<Vector3> spawns = new List<Vector3>();
        string[] lines = mapString.Split('\n');
        // initialize map where the size is given by the mapString times two for symmetry (incr by two for border walls)
        int xSize = (lines[0].Length * 2 - 1) + 2;
        int ySize = (lines.Length * 2 - 1) + 2;
        _currentMap = new HashSet<GameObject>[xSize, ySize];
        
        // create symmetric map
        for (int y = 0; y < lines.Length + 1; y++)
        {
            for (int x = 0; x < lines[0].Length + 1; x++)
            {
                // create border wall
                if (x == 0 || y == 0)
                {
                    createSymmeryPrefab(x, y, xSize, ySize, WallPrefab, false);
                    continue;
                }

                // create prefabs depending on string input
                char symbol = lines[y-1][x-1];
                switch (symbol)
                {
                    case 'x':
                        spawns.Add(new Vector3(x, 0, y));
                        if (xSize - x - 1 != x) spawns.Add(new Vector3(xSize-x-1, 0, y));
                        if (ySize - y - 1 != y) spawns.Add(new Vector3(x, 0, ySize-y-1));
                        if (xSize - x - 1 != x && ySize - y - 1 != y) spawns.Add(new Vector3(xSize-x-1, 0, ySize-y-1));
                        break;
                    case '+':
                        createSymmeryPrefab(x, y, xSize, ySize, WallPrefab, false);
                        break;
                    case '-':
                        createSymmeryPrefab(x, y, xSize, ySize, DestructableWallPrefab);
                        break;
                }
            }
        }
        
        return spawns;
    }

    /// <summary>
    /// Helper function to create the prefabs symmetrically
    /// </summary>
    private void createSymmeryPrefab(int x, int y, int xSize, int ySize, GameObject prefab, bool dynamic = true)
    {
        // make sure that a prefab is not placed two times at the same pos
        CreatePrefab(x, y, prefab, dynamic);
        if (xSize - x - 1 != x) CreatePrefab(xSize - x-1, y, prefab, dynamic);
        if (ySize - y - 1 != y) CreatePrefab(x, ySize - y-1, prefab, dynamic);
        if (xSize - x - 1 != x && ySize - y - 1 != y) CreatePrefab(xSize - x-1, ySize - y-1, prefab, dynamic);
    }

    /// <summary>
    /// clears current map
    /// </summary>
    private void destroyMap()
    {
        foreach(Transform parent in new Transform[] { StaticContainer, DynamicContainer })
        {
            foreach(Transform child in parent)
            {
                Destroy(child.gameObject);
            }
        }
    }

    /// <summary>
    /// Create a given prefab at a given position
    /// </summary>
    /// <param name="x">x pos of prefab</param>
    /// <param name="y">y pos of prefab</param>
    /// <param name="prefab">given prefab</param>
    /// <param name="dynamic">is the prefab a dynamic object (default true)</param>
    public GameObject CreatePrefab(int x, int y, GameObject prefab, bool dynamic = true)
    {
        GameObject obj = Instantiate(prefab, new Vector3(x, 0, y), prefab.transform.rotation, dynamic == true ? DynamicContainer : StaticContainer);
        Add(x, y, obj);
        return obj;
    }

    /// <summary>
    /// Returns all players alive
    /// </summary>
    /// <returns>set of players alive</returns>
    public HashSet<GameObject> GetPlayers()
    {
        return _currentPlayers;
    }
}
