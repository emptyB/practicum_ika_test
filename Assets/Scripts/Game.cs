﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This class can create new games and maintains the current game state
/// </summary>
public class Game : MonoBehaviour {
    /// <summary>
    /// maximum number of players (should always be 4?!)
    /// </summary>
    public const uint MAX_PLAYERS = 4;

    /// <summary>
    /// Prefab for a player
    /// </summary>
    public GameObject PlayerPrefab;

    /// <summary>
    /// Top-Down game camera
    /// </summary>
    public Transform GameCamera;

    /// <summary>
    /// The Map script to start a new game and update the games status
    /// </summary>
    public Map Map;

    /// <summary>
    /// UI Panel for the GameOver screen
    /// </summary>
    public Transform GameOverUI;

    /// <summary>
    /// UI Text to display the round time
    /// </summary>
    public Text TimeUI;

    /// <summary>
    /// Sets the amount of human players in a new game
    /// The amount has to be between 1 and MAX_PLAYERS
    /// </summary>
    public uint HumanPlayerCount = 2;

    /// <summary>
    /// Sets the amount of AI players in a new game
    /// HumanPlayerCount + AIPlayerCount <= MAX_PLAYERS
    /// </summary>
    public uint AIPlayerCount = 0;


    /// <summary>
    /// The maximum amount of time per Round in seconds
    /// </summary>
    public float RoundTime = 60;

    /// <summary>
    /// Available player colors
    /// </summary>
    public Color[] PlayerColors = new Color[4];

    /// <summary>
    /// Available game statuses
    /// </summary>
    private enum GameStatus
    {
        Paused,
        OnGoing,
        Victory,
        Draw,
        Defeat,
    }

    /// <summary>
    /// Current Game status
    /// </summary>
    private GameStatus _currentStatus = GameStatus.Paused;
    

    /// <summary>
    /// Start with a new Game (Changes if UI Menu exists)
    /// </summary>
    void Start () {
        StartNewGame();
	}

    private float _currentGameTime;
    /// <summary>
    /// Update game status
    /// Check for roundtime and players health status
    /// </summary>
    void Update () {

        switch (_currentStatus)
        {
            case GameStatus.Paused:
                return;
            case GameStatus.OnGoing:
                HashSet<GameObject> players = Map.GetPlayers();
                if (players.Count == 1)
                {
                    // check if last player is a bot or a human player => defeat / victory
                    GameObject[] playerArray = new GameObject[1];
                    players.CopyTo(playerArray);
                    _currentStatus = playerArray[0].GetComponent<UserInput>() == null ? GameStatus.Defeat : GameStatus.Victory;
                    gameOver(playerArray[0].GetComponent<Player>().Name + " won the game!");
                }
                if (players.Count == 0)
                {
                    // no one alive? => draw
                    _currentStatus = GameStatus.Draw;
                    gameOver("All player died!");
                }

                _currentGameTime += Time.deltaTime;
                TimeUI.text = string.Format("{0:00}:{1:00}", Mathf.Floor((RoundTime - _currentGameTime) / 60), Mathf.Floor((RoundTime - _currentGameTime) % 60));
                if (_currentGameTime > RoundTime)
                {
                    // time is up
                    _currentStatus = GameStatus.Draw;
                    gameOver("No more time left!");
                }
                break;
        }
    }

    /// <summary>
    /// Starts a new game with given settings
    /// Uses either default map (or later randomized map)
    /// </summary>
    public void StartNewGame()
    {
        Time.timeScale = 1;

        // create the map and recieve player spawns
        List<Vector3> spawns = Map.CreateMap(Map.DEFAULT_MAPSTRING);

        // update player settings (correct input errors ;) )
        HumanPlayerCount = (uint)Mathf.Max(1, Mathf.Min(MAX_PLAYERS, HumanPlayerCount));
        AIPlayerCount = (uint)Mathf.Min(AIPlayerCount, MAX_PLAYERS - HumanPlayerCount);

        uint playercount = HumanPlayerCount + AIPlayerCount;

        // if we allow more than 4 players we need a map with enough spawns
        if (playercount > spawns.Count || playercount > PlayerColors.Length) Debug.LogError("Need more spawns or more colors");


        // spawn players
        for(int i=0; i < playercount; i++)
        {
            GameObject player = Map.CreatePrefab(Mathf.RoundToInt(spawns[i].x), Mathf.RoundToInt(spawns[i].z), PlayerPrefab);
            player.GetComponentInChildren<Renderer>().material.color = PlayerColors[i];
            // add scripts for players either user or AI and set the name
            // TODO allow custom names
            if (i < HumanPlayerCount)
            {
                player.GetComponent<Player>().Name = "Player " + (i+1);
                player.AddComponent<UserInput>().Id = i+1;
            }
            else
            {
                player.GetComponent<Player>().Name = "Bot " + (HumanPlayerCount - i + 1);
                player.AddComponent<AIInput>();
            }
        }


        // compute the center of the generated map
        // Since all maps are symmetric w.r.t. x- and y-Axis the mean of all spawns is the center of the map (assuming we have at least one spawn)
        Vector3 center = Vector3.zero;
        for (int i = 0; i < spawns.Count; i++) center += spawns[i];
        center /= spawns.Count;
        // set camera 2D position to center keep height
        GameCamera.position = center + Vector3.up * GameCamera.position.y;
        

        _currentStatus = GameStatus.OnGoing;
        _currentGameTime = 0;
    }


    /// <summary>
    /// Called if the game is over
    /// Displays Victory, Draw, Defeat depending on current game status and additional the given message
    /// </summary>
    /// <param name="message">additional game over message (the cause)</param>
    private void gameOver(string message)
    {
        // Display GameOver status
        GameOverUI.gameObject.SetActive(true);
        GameOverUI.Find("Text_Heading").GetComponent<Text>().text = _currentStatus.ToString();
        GameOverUI.Find("Text_Cause").GetComponent<Text>().text = message;


        _currentStatus = GameStatus.Paused;
        Time.timeScale = 0;
    }
}
