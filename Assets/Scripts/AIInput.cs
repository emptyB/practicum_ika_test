﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// AI that computes the input for the player
/// </summary>
[RequireComponent(typeof(Player))]
public class AIInput : MonoBehaviour {



    private Map _map;
    private Player _player;
    

	// Use this for initialization
	void Start () {
        _map = GameObject.FindGameObjectWithTag("Map").GetComponent<Map>();
        _player = GetComponent<Player>();
	}


    
    /// <summary>
    /// Move according to threat levels
    /// </summary>
    void Update()
    {
        if (_player.IsMoving) return;

        int x = Mathf.RoundToInt(transform.position.x);
        int y = Mathf.RoundToInt(transform.position.z);
        
        // new direction with according threat level
        SortedList<float, Vector3> newDirections = new SortedList<float, Vector3>(new DuplicateKeyComparer<float>());

        newDirections.Add(threatLevelAt(x, y), Vector3.zero);
        if (!collision(x + 1, y)) newDirections.Add(threatLevelAt(x + 1, y), Vector3.right);
        if (!collision(x - 1, y)) newDirections.Add(threatLevelAt(x - 1, y), Vector3.left);
        if (!collision(x, y + 1)) newDirections.Add(threatLevelAt(x, y + 1), Vector3.forward);
        if (!collision(x, y - 1)) newDirections.Add(threatLevelAt(x, y - 1), Vector3.back);

        // move in dir with smallest threat (random if more)
        IList<float> sortedKeys = newDirections.Keys;
        float highestkey = sortedKeys[0];
        for(int i=0; i < sortedKeys.Count; i++)
        {
            // find number of smallest keys
            if (sortedKeys[i] > highestkey || i+1 == sortedKeys.Count)
            {
                _player.Move(newDirections.Values[Random.Range(0, i)]);
                break;
            }
        }
    }

    /// <summary>
    /// Comparer which allows same keys in dir
    /// </summary>
    /// <typeparam name="TKey"></typeparam>
    public class DuplicateKeyComparer<TKey> : IComparer<TKey> where TKey : System.IComparable
    {
        public int Compare(TKey x, TKey y)
        {
            int result = x.CompareTo(y);

            if (result == 0)
                return 1;   // Handle equality as beeing greater
            else
                return result;
        }
    }

    /// <summary>
    /// Check for collison at given position
    /// </summary>
    private bool collision(int x, int y)
    {
        foreach (GameObject obj in _map.GetCell(x, y))
        {
            if (obj.tag == "CollisionObject") return true;
        }
        return false;
    }

    /// <summary>
    /// Returns threat level at a given position
    /// </summary>
    /// <param name="x">x pos</param>
    /// <param name="y">y pos</param>
    /// <returns>threat level of pos</returns>
    private float threatLevelAt(int x, int y)
    {
        return threatLevelInDir(x, y, 1, 0) +
            threatLevelInDir(x, y, 0, 1) +
            threatLevelInDir(x, y, -1, 0) +
            threatLevelInDir(x, y, 0, -1);
    }

    /// <summary>
    /// Get threat level in direction given by point and direction
    /// </summary>
    /// <param name="x">x pos of point</param>
    /// <param name="y">y pos of point</param>
    /// <param name="xAdd">x direction (0 or 1)</param>
    /// <param name="yAdd">y direction (0 or 1)</param>
    /// <returns></returns>
    private float threatLevelInDir(int x, int y, int xAdd, int yAdd)
    {
        float threat = 0;
        for (int i=0; true; i++)
        {
            int curX = x + i * xAdd;
            int curY = y + yAdd * i;
            float dist = i;
            foreach (GameObject obj in _map.GetCell(curX, curY))
            {
                // update threat level if bomb or return if wall
                Bomb bomb = obj.GetComponent<Bomb>();
                if (bomb != null)
                {
                    // ignores chain reactions for now
                    if (dist <= bomb.GetExplosionRange)
                    {
                        // update threat based on distance and time of bomb
                        threat += (1 / Mathf.Max(dist, 1)) * 20 + (1 / Mathf.Max(bomb.ExplosionTime, 0.01f)) * 100;
                    }
                }
                else if (obj.tag == "CollisionObject" && obj.GetComponent<Player>() == null)
                {
                    // it's a wall - not elegant but works for now
                    return threat;
                }
            }
        }
    }
    



	
}
