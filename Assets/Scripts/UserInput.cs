﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This behaviour is specific for human players it transfers user input in actions of the player behaviour
/// </summary>
[RequireComponent(typeof(Player))]
public class UserInput : MonoBehaviour {

    /// <summary>
    /// User Id -> use for input
    /// </summary>
    [HideInInspector]
    public int Id;


    private Player _player;
	// Use this for initialization
	void Start () {
        _player = GetComponent<Player>();
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetAxis("Player" + Id + "_horizontal") < 0) _player.Move(Vector3.left);
        if (Input.GetAxis("Player" + Id + "_horizontal") > 0) _player.Move(Vector3.right);
        if (Input.GetAxis("Player" + Id + "_vertical") < 0) _player.Move(Vector3.back);
        if (Input.GetAxis("Player" + Id + "_vertical") > 0) _player.Move(Vector3.forward);

        if (Input.GetButtonDown("Player" + Id + "_bomb")) _player.PlaceBomb();
        if (Input.GetButtonDown("Player" + Id + "_remoteBomb")) _player.DetonateRemoteBomb();
    }
}
