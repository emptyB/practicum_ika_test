﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup : MonoBehaviour {


    /// <summary>
    /// Diffrent Properties a pickup can have
    /// </summary>
    public int IncreaseBombs = 0;
    public float SpeedMultiplier = 1;
    public int IncreaseBombRadius = 0;

    /// <summary>
    /// If remote bomb time is zero it will be ignored otherwise the player gains a remote bomb for the given time (in seconds)
    /// </summary>
    public float RemoteBombTime = 0;


    /// <summary>
    /// Function that applies the effects to the given player
    /// </summary>
    /// <param name="player">Player that gains the effects</param>
    public void PickUp(Player player)
    {
        player.BombCount += IncreaseBombs;
        player.Speed *= SpeedMultiplier;
        player.BombRadius += IncreaseBombRadius;
        player.RemoteBombTime += RemoteBombTime;
    }
}
