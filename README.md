# README #

# Bomberman 3D
* Unity Game
* Classic Bomberman in 3D
* Two player couch coop

## Setup
* You need Unity3D (5.6.2f1 or higher)
* Clone the repository
* Open the file /Assets/main.exe (with Unity3D)
* In Unity File -> Build And Run -> Build Settings -> Make sure "main" is listed in Scenes in Build (otherwise add open scenes) -> Build and Run

## Controls
### Player 1
* Movement: w,a,s,d
* Place bomb: left shift
* Remote bomb: left ctrl
### Player 2
* Movement: up,left,down,right
* Place bomb: right shift
* Remote bomb: right ctrl

## Settings
* Currently you can only change between one or two players and change the round time
* To change these Settings select the "Game" Node in the Hierachy tab and change the settings in the inspector

## How to Play
* Navigate your player through the "maze"
* You can destoy (destructable) walls and kill players by placing bombs which have a given explosion radius (default 1)
* Destructable walls can prop Pickups that can increase the player stats
* Goal: kill all players before the time runs out

## Pickups
#### Red
* Increases the number of available Bombs by one
#### Orange
* Increases the blast radius of bombs by one
#### Green
* Multiplies the current player speed by 1.25
#### Purple
* Give the player access to a remote bomb for 10 seconds (stackable)

## Future Work
### Content
#### Pickups
* Add more (= more fun)
* (Introduce lifes per player (can be hit three times))
#### AI
* Finish current basic AI
* Place Bombs (+intelligent)
* Try to win
#### Map Generation
* Randomize maps (structure allows simple integration)
### Performance
* Recycle GameObjects instead of create and destroy
### GUI
* Add main menu
* Set Settings via GUI
### Visuals
* Repace placeholders with better 3D models
* Add Textures
* No default GUI elements

## About
* Created by Michael Burkart
* As part of a practicum application as Unity Programmer at IKA